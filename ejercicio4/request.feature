@baseUrl @baseUrl-cleverIT
Feature: Validate a request JSON from an HTTP endpoint(buy order)
  Scenario: Validate agreement field exists in the request
    Given The json request data
      """json
      {
      "agreement": 1,
      "cardType": 1,
      "amount": 20000,
      "expirationDate": "2020-01-31",
      "storeId": 1037,
      "dni": "111111111",
      "sku": "26",
      "order": 30991,
      "subOrder": 2601
      }
      """
    And The property "agreement" is a number
    Then is a valid request

  Scenario: Check a valid range of the agreement field value
    Given The json request data
      """json
      {
      "agreement": 1,
      "cardType": 1,
      "amount": 20000,
      "expirationDate": "2020-01-31",
      "storeId": 1037,
      "dni": "111111111",
      "sku": "26",
      "order": 30991,
      "subOrder": 2601
      }
      """
    And The property "agreement" is a number between 1 and 10
    Then is a valid request

  Scenario: Advanced confirmation of the relationship between a valid card type and agreement fields
    Given The json request data
      """json
      {
      "agreement": 1,
      "cardType": 1,
      "amount": 20000,
      "expirationDate": "2020-01-31",
      "storeId": 1037,
      "dni": "111111111",
      "sku": "26",
      "order": 30991,
      "subOrder": 2601
      }
      """
    And The property "agreement" and "cardType" are equals
    Then is a valid request

  Scenario: ensure buy order includes a valid DNI number
    Given The json request data
      """json
      {
      "agreement": 1,
      "cardType": 1,
      "amount": 20000,
      "expirationDate": "2020-01-31",
      "storeId": 1037,
      "dni": "111111111",
      "sku": "26",
      "order": 30991,
      "subOrder": 2601
      }
      """
    And The property "dni" length is 9
    Then is a valid request

  Scenario: Check request(buy order) is not already expired
    Given The json request data
      """json
      {
      "agreement": 1,
      "cardType": 1,
      "amount": 20000,
      "expirationDate": "2020-01-31",
      "storeId": 1037,
      "dni": "111111111",
      "sku": "26",
      "order": 30991,
      "subOrder": 2601
      }
      """
    And The property "expirationDate" is greather than today
    Then is a valid request

  Scenario: Generate a valid order without stock
    Given The json request data
      """json
      {
      "agreement": 1,
      "cardType": 1,
      "amount": 20000,
      "expirationDate": "2020-01-31",
      "storeId": 1037,
      "dni": "111111111",
      "sku": "26",
      "order": 30991,
      "subOrder": 2601
      }
      """
    And The property "expirationDate" is greather than today
    When is a valid request
    Then The response is "order without stock"
    And The order "status" should be "rejected"
