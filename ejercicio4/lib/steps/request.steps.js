'use strict';

const moment = require('moment');

module.exports = function() {

  this.Then(/^The order "([^"]*)" should be "([^"]*)"$/, function(arg1, arg2, callback) {
    //simulated rejected order
    callback(null, 'rejected order');
  });

  this.Then(/^The response is "([^"]*)"$/, function(arg1, callback) {
    //simulated HTTP response
    callback(null, 'order without stock');
  });

  this.Given(/^The property "([^"]*)" is greather than today$/, function(arg1, callback) {
    this.requestBody = this.requestBody || {};
    if (moment(this.requestBody[arg1], 'YYYY-MM-DD').isAfter(moment())) {
      callback();
    } else {
      callback(new Error('invalid date'));
    }
  });

  this.Given(/^The property "([^"]*)" length is (\d+)$/, function(arg1, arg2, callback) {
    this.requestBody = this.requestBody || {};
    if (this.requestBody[arg1].length == arg2) {
      callback();
    } else {
      callback(new Error('not valid'));
    }
  });

  this.Given(/^The property "([^"]*)" and "([^"]*)" are equals$/, function(arg1, arg2, callback) {
    this.requestBody = this.requestBody || {};
    if (this.requestBody[arg1] == this.requestBody[arg2]) {
      callback();
    } else {
      callback(new Error('not valid'));
    }
  });

  this.Given(/^The property "([^"]*)" is a number$/, function(arg1, callback) {
    this.requestBody = this.requestBody || {};
    if (isNaN(this.requestBody[arg1])) {
      callback(new Error('not a number'));
    } else {
      callback();
    }
  });

  this.Given(/^The property "([^"]*)" is a number between (\d+) and (\d+)$/, function(arg1, arg2, arg3, callback) {
    this.requestBody = this.requestBody || {};
    if (this.requestBody[arg1] >= arg2 && this.requestBody[arg1] <= arg3) {
      callback();
    } else {
      callback(new Error('not valid'));
    }
  });

  this.Given(/^The json request data$/i, function(data) {
    this.requestBody = JSON.parse(data);
  });

  this.Then(/^is a valid request$/, function(callback) {
    //simulated successful POST
    callback(null, 'valid order');
  });

};
