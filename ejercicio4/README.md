## Cucumber BDD using Gherkin

Test Cases File: [request.feature](https://bitbucket.org/gary_velasquez/automation-test-repo/src/master/ejercicio4/request.feature/)  
Step Functions File: [./lib/steps/request.steps.js ](https://bitbucket.org/gary_velasquez/automation-test-repo/src/master/ejercicio4/lib/steps/request.steps.js/)   

## Installation requirements:

```bash
Cucumber 1.0
NodeJS v12.14.1
```

## Install dependencies:

```bash
$ npm install
```
## How to run:

```bash
$ npm test request.feature
```

## example output (6 cases):

![Image1](https://nodality-public-files.s3.amazonaws.com/t1.png "Image1")


![Image2](https://nodality-public-files.s3.amazonaws.com/t2.png "Image2")
