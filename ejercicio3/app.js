"use strict"
const puppeteer = require("puppeteer");
const path = require("path");
const screnshotFolder = "./screenshots";

const init = async (searchTerm) => {
  const SEARCH_INPUT = "#tsf > div:nth-child(2) > div.A8SBwf > div.RNNXgb > div > div.a4bIc > input";
  const BODY_SELECTOR = "#rcnt";

  const {browser, page} = await startBrowser();
  //browse to current page and take screenshots of the site flow
  await page.goto("https://google.cl", {waitUntil: "networkidle0"});
  await page.waitForSelector(SEARCH_INPUT);
  await page.screenshot({path: path.join(screnshotFolder, "home.png")});
  await page.focus(SEARCH_INPUT);

  await page.keyboard.type(searchTerm);
  await page.screenshot({path: path.join(screnshotFolder, "search-term.png")});
  await page.keyboard.press("Enter");
  await page.waitForSelector(BODY_SELECTOR);
  await page.screenshot({path: path.join(screnshotFolder, "completed-search.png")});
  const html = await page.content();
  //close browser instance
  await browser.close();
}

//start headless browser configuration
const startBrowser = async () =>{
  const browser = await puppeteer.launch({headless: false, ignoreHTTPSErrors: true});
  const page = await browser.newPage();
  await page.setViewport({width: 1920, height: 1080});
  await page.setDefaultNavigationTimeout(60000);
  return {browser, page};
}

init("web development in windows");
