
## Test Case implementation with Puppeteer (Test Automation Tool):

**Feature:** Advanced Google Search  
  As an advanced internet user I need to search for a specific term placing in it between quotes.  
  **Scenario:** Specific term Google search.  
   **Given** Web browser is on the Google Search page.  
    **When** the search phrase "web development in windows" is entered between quotes.  
    **Then** results for "web development in windows" are shown.  
    **But** the results do not include "web development in linux”.  

## Installation requirements:

```bash
NodeJS v12.14.1
```

## Install dependencies:

```bash
$ npm install
```
## How to run:

```bash
$ node app.js
```

## example output:

Initial state:

![Image1](https://nodality-public-files.s3.amazonaws.com/home.png "Image1")

Search:

![Image2](https://nodality-public-files.s3.amazonaws.com/search-term.png "Image2")

Results:

![Image3](https://nodality-public-files.s3.amazonaws.com/completed-search.png "Image3")
