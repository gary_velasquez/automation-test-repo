## Installation requirements:

```bash
JMeter 5.2.1
Java 8+
```

## Tests

current configuration:

![Image1](https://nodality-public-files.s3.amazonaws.com/config1.png "Image1")

![Image2](https://nodality-public-files.s3.amazonaws.com/config2.png "Image2")
