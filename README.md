# Automation Test Repo


## Ejercicio 1:

Elige 3 funcionalidades de Google Search y selecciona 3 casos de prueba usando gherkins

### Respuesta:

[Link](https://nodality-public-files.s3.amazonaws.com/Gherkin-GSF.pdf)

## Ejercicio 2:

Realiza un caso de prueba de stress lanzando 1000 request para una subida de 10 segundos al siguiente endpoint de lectura: https://jsonplaceholder.typicode.com/posts/1/comments . Y agregar el archivo jmx a la raíz del proyecto.

[Link](https://bitbucket.org/gary_velasquez/automation-test-repo/src/master/ejercicio2/)

## Ejercicio 3:

Automatizar uno de los casos realizados en Gherkins en el ejercicio 1, usando el lenguaje y la herramienta que desees

[Link](https://bitbucket.org/gary_velasquez/automation-test-repo/src/master/ejercicio3/)

## Ejercicio 4:

Dado el siguiente JSON request:

```bash
{
    "agreement": 1,  
    "cardType": 1,  
    "amount": 20000,  
    "expirationDate": "2020-01-31",    
    "storeId": 1037,  
    "dni": "111111111",  
    "sku": "26",  
    "order": 30991,  
    "subOrder": 2601  
}
```

Se espera realizar al menos 6 casos de prueba para poder validar los posibles parámetros de entrada y las respuestas que podría entregar la API. Los casos pueden ser creados como Gherkins o en un .txt y adjuntarlo en la raíz del proyecto

Para la presentación de los resultados, debe realizar un git clone de nuestro repo, crear una Branch y luego crear el PR hacia develop

[Link](https://bitbucket.org/gary_velasquez/automation-test-repo/src/master/ejercicio4/)
